<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->insert([
            [
                'name' => 'Euro',
                'iso_code' => 'EUR',
            ],
            [
                'name' => 'United States Dollar',
                'iso_code' => 'USD',
            ],
            [
                'name' => 'Turkish lira',
                'iso_code' => 'TRY',
            ],
            [
                'name' => 'Pound sterling',
                'iso_code' => 'GBP',
            ],
        ]);
    }
}
