<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Exchange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange', function(Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('currency_from_id');
            $table->unsignedBigInteger('currency_to_id');
            $table->decimal('rate', 8, 2);
            $table->timestamps();

            $table->foreign('currency_from_id')->references('id')->on('currencies');
            $table->foreign('currency_to_id')->references('id')->on('currencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange');
    }
}
