<?php

namespace App\Providers;

use App\Models\Currency;
use App\Models\Exchange;
use App\Exceptions\CurrencyException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use stdClass;

class ExchangeProvider
{
    /**
     *
     * CREATE
     *
     */

    /**
     *
     * READ
     *
     */

    /**
     * Gets currency by iso code
     *
     * @param string $isoCode
     * @return Currency
     */
    public static function getCurrencyByIsoCode(string $isoCode): Currency
    {
        try {
            return Currency::where('iso_code', $isoCode)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            throw new CurrencyException();
        }
    }

    /**
     * Gets exchange rate
     *
     * @param Currency $from
     * @param Currency $to
     * @return Exchange
     */
    public static function getExchangeRate(Currency $from, Currency $to): Exchange
    {
        $exchange = Exchange::where([
            'currency_from_id' => $from->id,
            'currency_to_id' => $to->id
        ])->first();

        if ($exchange === null) {
            $exchange = self::updateOrCreateExchangeRate(
                $from->id,
                $to->id,
                self::getExchangeRateFromApi($from->iso_code, $to->iso_code)->info->rate
            );
        }

        return $exchange;
    }

    /**
     * Gets exchange rate from api
     *
     * @param string $fromCode
     * @param string $toCode
     * @return void
     */
    private static function getExchangeRateFromApi(string $fromCode, string $toCode): stdClass
    {
        return json_decode(file_get_contents(env("EXCHANGE_RATE_API") . "?from=$fromCode&to=$toCode"));
    }

    /**
     *
     * UPDATE
     *
     */

    /**
     * Updates or creates exchange rate
     *
     * @param integer $fromId
     * @param integer $toId
     * @param float $rate
     * @return Exchange
     */
    private static function updateOrCreateExchangeRate(int $fromId, int $toId, float $rate): Exchange
    {
        return Exchange::updateOrCreate(
            [
                'currency_from_id' => $fromId,
                'currency_to_id' => $toId,
                'rate' => $rate
            ],
            [
                'currency_from_id' => $fromId,
                'currency_to_id' => $toId
            ]
        );
    }

    /**
     *
     * DELETE
     *
     */
}
