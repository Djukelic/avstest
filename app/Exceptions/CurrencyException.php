<?php

namespace App\Exceptions;

class CurrencyException extends \Exception
{
    public function __construct()
    {
        parent::__construct("Wrong currency", 0);
    }
}
