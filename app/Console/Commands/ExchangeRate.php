<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Providers\ExchangeProvider;

class ExchangeRate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exchange:rate {from} {to}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gives exchange rate between two currencies';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            echo ExchangeProvider::getExchangeRate(
                ExchangeProvider::getCurrencyByIsoCode($this->argument('from')),
                ExchangeProvider::getCurrencyByIsoCode($this->argument(('to')))
            )->rate . "\n";
        } catch (\App\Exceptions\CurrencyException $e) {
            echo $e->getMessage() . "\n";
        }
    }
}
